package kopitiamapp.com.kopitiam;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rjirwanshah on 10/09/2016.
 * Updated by kamri on 27/09/2016
 */
public class LocationSearchBasedResult extends Fragment {

    public static final String TAG = "TAG";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.location_search_based_result, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume: showing on screen");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "onPause: dismissing from screen");
    }
}
