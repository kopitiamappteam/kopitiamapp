package kopitiamapp.com.kopitiam.api;

import com.android.volley.toolbox.JsonObjectRequest;
import kopitiamapp.com.kopitiam.controller.UserController;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by Loo on 10/11/2016.
 */
public class BackendlessJsonObjectRequest extends JsonObjectRequest {

    private final Map<String, String> mAdditionalHeaders = new HashMap<>();

    public BackendlessJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public void putHeader(String key, String value) {
        mAdditionalHeaders.put(key, value);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        // Set up required headers
        Map<String, String> headers = new HashMap<>(super.getHeaders());
        headers.put("application-id", "56EC6F65-F5FC-2EB4-FF60-5B0167900700");
        headers.put("secret-key", "40CB3C0A-4AE1-7639-FFD7-F77AB7F62100");
        headers.put("application-type", "REST");

        headers.putAll(mAdditionalHeaders);

        return headers;
    }
}

