package kopitiamapp.com.kopitiam.Model;

/**
 * Created by Loo on 10/08/2016.
 */
public class Rating {

    private Integer mKopiTaste;
    private Integer mKopiVariety;
    private Integer mAtmospheres;
    private Integer mRatingScore;
    private String mUserId;
    private String mKopitiamID;

    public Rating(Integer KopiTaste, Integer KopiVariety) {
        mKopiTaste = KopiTaste;
        mKopiVariety = KopiVariety;
    }

    public Integer getKopiTaste() {  return mKopiTaste;    }
    public Integer getKopiVariety() {
        return mKopiVariety;
    }
    public Integer getAtmospheres() {
        return mAtmospheres;
    }
    public Integer getRatingScore() {
        return mRatingScore
    }
    public String getUserId() {return mUserId;    }
    public String getKopitiamID() {return mKopitiamID;    }
}
