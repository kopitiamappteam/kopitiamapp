package kopitiamapp.com.kopitiam.Model;

/**
 * Created by Loo on 10/08/2016.
 */
public class Review {

    private String mReviewComment;
    private String mReviewBy;
    private String mKopitiamID;

    public Review(String ReviewComment, String ReviewBy, String KopitiamID) {
        mReviewComment = ReviewComment;
        mReviewBy = ReviewBy;
        mKopitiamID = KopitiamID;
    }

    public String getReviewComment() {  return mReviewComment;    }
    public String getReviewBy() {
        return mReviewBy;
    }
    public String getKopitiamID() {
        return mKopitiamID;
    }



}
