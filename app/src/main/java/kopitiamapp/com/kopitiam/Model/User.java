package kopitiamapp.com.kopitiam.Model;

/**
 * Created by Loo on 10/11/2016.
 */
public class User {

    private String mUserId;
    private String mUserPassword;
    private String mUserProfilePhoto;
    private String mUserLoginName;

    public User(String UserId, String UserPassword, String UserProfilePhoto, String UserLoginName) {
        mUserId = UserId;
        mUserPassword = UserPassword;
        mUserProfilePhoto = UserProfilePhoto;
        mUserLoginName = UserLoginName;
    }

    public String getUserId() {  return mUserId;    }
    public String getUserPassword() {  return mUserPassword;    }
    public String getUserProfilePhoto() {
        return mUserProfilePhoto;
    }
    public String getUserLoginName() {
        return mUserLoginName;
    }
}
