package kopitiamapp.com.kopitiam.Model;

/**  private String mLocationState;
 private String mLocationBranch;
 private String mLocationAddress;
 private String mKopitiamID;

 * Created by Loo on 10/08/2016.
 */
public class Location {

    private String mLocationState;
    private String mLocationBranch;
    private String mLocationAddress;
    private String mKopitiamID;


    public Location(String LocationState, String LocationBranch, String LocationAddress, String KopitiamID) {
        mLocationState = LocationState;
        mLocationBranch = LocationBranch;
        mLocationAddress = LocationAddress;
        mKopitiamID = KopitiamID;
    }

    public String getLocationState() {  return mLocationState;    }
    public String getLocationBranch() {
        return mLocationBranch;
    }
    public String getKopitiamID() {
        return mKopitiamID;
    }
}
