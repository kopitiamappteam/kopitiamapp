package kopitiamapp.com.kopitiam.Model;

/**
 * Created by Jeremyee on 08/10/2016.
 */

public class Kopitiam {
    private String mName;
    private String mID;
    private String mDescription;
    private String mAddress;
    private String mPhoneNumber;
    private boolean mHasWifi;
    private String mWebsiteURL;
    private String mMenuURL;

    public Kopitiam(String name, String ID) {
        mName = name;
        mID = ID;
    }

    public String getName() {  return mName;    }
    public String getId() {
        return mID;
    }
    public String getDescription() {
        return mDescription;
    }
    public String getAddress() {
        return mAddress;
    }
    public String getPhoneNumber() {return mPhoneNumber;    }
    public Boolean getHasWifi() {
        return mHasWifi;
    }
    public String getWebsiteURL() {return mWebsiteURL;    }
    public String getMenuURL() {return mMenuURL;    }

}