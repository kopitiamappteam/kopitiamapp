package kopitiamapp.com.kopitiam;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by rjirwanshah on 06/09/2016.
 */
public class SearchFragment extends Fragment implements View.OnClickListener{


    private TextView mLocation;
    private TextView mKopitiam;
    private Button mBtnLocation;

    public SearchFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.search_location_fragment, container, false);

        mLocation = (TextView) rootView.findViewById(R.id.cyberjayaId);
        mKopitiam = (TextView) rootView.findViewById(R.id.hipsterId);
        mBtnLocation = (Button) rootView.findViewById(R.id.btn_klcc_id);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Clickable TextView
//        this.mLocation = (TextView) view.findViewById(R.id.hipsterId);
//        this.mLocation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SearchFragment.this.showLocationSearchBasedResult();
//            }
//        });
        this.mBtnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationSearchBasedResult();
            }
        });
    }

    public void showLocationSearchBasedResult(){

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg, new LocationSearchBasedResult())
                .addToBackStack("LocationSearchBasedResult")
                .commit();
    }

    @Override
    public void onClick(View v) {

    }
}
